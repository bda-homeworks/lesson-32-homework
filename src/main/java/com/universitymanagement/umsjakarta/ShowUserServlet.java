package com.universitymanagement.umsjakarta;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.*;

@WebServlet("/showdata")
public class ShowUserServlet extends HttpServlet {
    private final static String query = "select id,name,email,mobile,dob,city,gender from user";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        //getting writer
        PrintWriter pw = resp.getWriter();
        //setting content type
        resp.setContentType("text/html");
        //link to the bootstrap
        pw.println("<link rel='stylesheet' href='css/bootstrap.css'></link>");
        //load to JDBC driver
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (Exception e) {
            e.printStackTrace();
        }

        try(Connection con = DriverManager.getConnection("jdbc:mysql:///umsservlet","root","root")) {
            PreparedStatement ps = con.prepareStatement(query);

            //resultSet
            ResultSet rs = ps.executeQuery();
            pw.println("<table class='table table-hover table-striped'>");
            pw.println("<tr>");
            pw.println("<th>ID</th>");
            pw.println("<th>Name</th>");
            pw.println("<th>Email</th>");
            pw.println("<th>Date of Birth</th>");
            pw.println("<th>City</th>");
            pw.println("<th>Gender</th>");
            pw.println("</tr>");
            while (rs.next()) {
                pw.println("<tr>");
                pw.println("<td>"+ rs.getInt(1) +"</td>");
                pw.println("<td>"+ rs.getInt(2) +"</td>");
                pw.println("<td>"+ rs.getInt(3) +"</td>");
                pw.println("<td>"+ rs.getInt(4) +"</td>");
                pw.println("<td>"+ rs.getInt(5) +"</td>");
                pw.println("<td>"+ rs.getInt(6) +"</td>");
                pw.println("<td>"+ rs.getInt(7) +"</td>");
                pw.println("</tr>");
            }
            pw.println("</table>");
        } catch (SQLException se) {
            se.printStackTrace();
        }
    }
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req,resp);
    }
}