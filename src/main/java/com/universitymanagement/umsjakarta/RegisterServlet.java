package com.universitymanagement.umsjakarta;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

@WebServlet("/register")
public class RegisterServlet extends HttpServlet {

    private final static String query = "insert into user(name, email, mobile, dob, city, gender) values(?,?,?,?,?,?)";
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //get PrintWriter
        PrintWriter pw = resp.getWriter();
        //set content type
        resp.setContentType("text/html");
        //link the bootstrap
        pw.println("<link rel='stylesheet' href='css/bootstrap.css' ></link>");
        //get the values
        String name = req.getParameter("userName");
        String email = req.getParameter("email");
        String mobile = req.getParameter("mobile");
        String dob = req.getParameter("dob");
        String city = req.getParameter("city");
        String gender = req.getParameter("gender");
        // Loading database JDBC
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (Exception e) {
            e.printStackTrace();
        }
        try(Connection con = DriverManager.getConnection("jdbc:mysql:///umsservlet","root","root")) {
            PreparedStatement ps = con.prepareStatement(query);
            //setting the values
            ps.setString(1,name);
            ps.setString(2,email);
            ps.setString(3,mobile);
            ps.setString(4,dob);
            ps.setString(5,city);
            ps.setString(6,gender);
            //execution of query
            pw.println("<div class = 'card' style='margin:auto;width:300px;margin-top:100px'>");
            int count = ps.executeUpdate();
            if (count == 1) {
                pw.println("<h2 class='bg-danger text-light text-center'>Record Registered Successfully</h2>");
            } else {
                pw.println("<h2 class='bg-danger text-light text-center'>Record Not Registered!</h2>");
            }
        } catch (SQLException se) {
            se.printStackTrace();
        }
        pw.println("<a href='index.jsp'><button class='btn btn-outline-success'>Home</button></a>");
        pw.println("</div>");
        //close the stream
        pw.close();
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req,resp);
    }
}
